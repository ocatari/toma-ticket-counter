package es.tomaticket.apps.tomaticketcounter.Fragments;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import es.tomaticket.apps.tomaticketcounter.ApiRequests.GetEventCountRequestTask;
import es.tomaticket.apps.tomaticketcounter.ApiRequests.OnApiRequestTaskCompleted;
import es.tomaticket.apps.tomaticketcounter.ApiRequests.ResetEventCountRequestTask;
import es.tomaticket.apps.tomaticketcounter.ApiRequests.Response;
import es.tomaticket.apps.tomaticketcounter.ApiRequests.SubtractEventCountRequestTask;
import es.tomaticket.apps.tomaticketcounter.MainActivity;
import es.tomaticket.apps.tomaticketcounter.R;
import es.tomaticket.apps.tomaticketcounter.Utils.Style;
import es.tomaticket.apps.tomaticketcounter.ViewModels.MainActivityViewModel;


public class ResetFragment extends Fragment {

    Button reset;
    TextView count;
    TextView event;
    ProgressBar progressBar;
    TextInputLayout textInputLayout;

    MainActivityViewModel model;
    MainActivity activity;
    Handler handler = new Handler();
    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            Log.e("Handler", "Run");
            updateCount();

            handler.postDelayed(this, 5000);
        }
    };

    public ResetFragment() {
        // Required empty public constructor
    }


    public static ResetFragment newInstance() {
        ResetFragment fragment = new ResetFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();

        model = new ViewModelProvider(requireActivity()).get(MainActivityViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reset, container, false);
        initView(view);

        handler.post(runnableCode);

        final Vibrator v = (Vibrator) activity.getSystemService(Context.VIBRATOR_SERVICE);
        final MediaPlayer mp = MediaPlayer.create(activity, R.raw.beep);

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (model.getIdEvent() == MainActivityViewModel.NO_EVENT_ID) {
                    Toast.makeText(activity, "Necesario selecionar evento", Toast.LENGTH_SHORT).show();
                    return;
                }
                disableInteraction();
                String pass = textInputLayout.getEditText().getText().toString();
                String id = Integer.toString(model.getIdEvent());

                ResetEventCountRequestTask resetEventCountRequestTask = new ResetEventCountRequestTask(id, pass);
                resetEventCountRequestTask.setListener(new OnApiRequestTaskCompleted() {
                    @Override
                    public void onApiRequestTaskSucceed(Object object) {
                        Integer c = (Integer) object;
                        count.setText(Integer.toString(c));

                        if (mp.isPlaying()) {
                            mp.pause();
                            mp.seekTo(0);
                        }

                        mp.start();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                        } else {
                            v.vibrate(500);
                        }
                        Toast.makeText(activity, "Contador establecido a 0", Toast.LENGTH_SHORT).show();
                        enableInteraction();
                        textInputLayout.getEditText().setText("");
                    }

                    @Override
                    public void onApiRequestTaskFailed(Response response) {
                        if (response.getCode() == 200)
                            Toast.makeText(activity, "Contraseña incorrecta", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(activity, "Petición fallida", Toast.LENGTH_SHORT).show();
                        enableInteraction();
                    }
                });
                resetEventCountRequestTask.execute();
            }
        });

        return view;
    }

    private void initView(View view) {
        reset = view.findViewById(R.id.btnReset);
        count = view.findViewById(R.id.tvCount);
        event = view.findViewById(R.id.tvEvent);
        textInputLayout = view.findViewById(R.id.tilPassword);
        progressBar = view.findViewById(R.id.progressBar);
        enableInteraction();

        int radious = 20;
        GradientDrawable pressedBg = Style.getStyle(R.color.colorPressedResetBtn, radious, activity);
        GradientDrawable inactiveBg = Style.getStyle(R.color.colorPressedResetBtn, radious, activity);
        GradientDrawable defaultBg = Style.getStyle(R.color.colorResetBtn, radious, activity);
        StateListDrawable stateListDrawable = Style.getStyleState(pressedBg, inactiveBg, defaultBg);
        reset.setBackground(stateListDrawable);

        ColorStateList colorStateList = Style.getStyleColorState(R.color.textColorPrimary, R.color.textColorPrimary, activity);
        reset.setTextColor(colorStateList);
    }

    private void updateCount() {
        if (model.getIdEvent() != MainActivityViewModel.NO_EVENT_ID) {
            String id = Integer.toString(model.getIdEvent());
            GetEventCountRequestTask getEventCountRequestTask = new GetEventCountRequestTask(id);
            getEventCountRequestTask.setListener(new OnApiRequestTaskCompleted() {
                @Override
                public void onApiRequestTaskSucceed(Object object) {
                    Integer c = (Integer) object;
                    count.setText(Integer.toString(c));

                }

                @Override
                public void onApiRequestTaskFailed(Response response) {

                }
            });
            getEventCountRequestTask.execute();

        }
        event.setText(model.getEvent());
    }

    private void disableInteraction() {
        progressBar.setVisibility(View.VISIBLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void enableInteraction() {
        progressBar.setVisibility(View.GONE);
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(runnableCode);

        super.onDestroy();
    }
}