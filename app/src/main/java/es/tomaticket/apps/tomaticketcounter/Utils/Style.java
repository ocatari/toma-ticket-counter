package es.tomaticket.apps.tomaticketcounter.Utils;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;

import es.tomaticket.apps.tomaticketcounter.MainActivity;

public class Style {

    public static GradientDrawable getStyle(int defaultColor, int radious, MainActivity activity) {
        GradientDrawable style = new GradientDrawable();
        style.setColor(activity.getResources().getColor(defaultColor));
        style.setCornerRadius(radious);
        return style;
    }

    public static StateListDrawable getStyleState(GradientDrawable pressedBg, GradientDrawable inactiveBg, GradientDrawable defaultBg) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[] {android.R.attr.state_pressed}, pressedBg);
        stateListDrawable.addState(new int[] {-android.R.attr.state_enabled}, inactiveBg);
        stateListDrawable.addState(new int[] {}, defaultBg);
        return stateListDrawable;
    }

    public static ColorStateList getStyleColorState(int textColor, int inactiveTextColor, MainActivity activity) {
        return new ColorStateList(
                new int [][] {
                        new int [] {android.R.attr.state_enabled},
                        new int [] {-android.R.attr.state_enabled},
                }, new int[] {
                activity.getResources().getColor(textColor),
                activity.getResources().getColor(inactiveTextColor),
        });
    }

}
