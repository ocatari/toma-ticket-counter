package es.tomaticket.apps.tomaticketcounter.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

import es.tomaticket.apps.tomaticketcounter.Adapters.MyAdapter;
import es.tomaticket.apps.tomaticketcounter.ApiRequests.GetEventsRequestTask;
import es.tomaticket.apps.tomaticketcounter.ApiRequests.OnApiRequestTaskCompleted;
import es.tomaticket.apps.tomaticketcounter.ApiRequests.Response;
import es.tomaticket.apps.tomaticketcounter.MainActivity;
import es.tomaticket.apps.tomaticketcounter.Models.EventItem;
import es.tomaticket.apps.tomaticketcounter.R;
import es.tomaticket.apps.tomaticketcounter.ViewModels.MainActivityViewModel;


public class EventFragment extends Fragment {
    RecyclerView recyclerView;
    ProgressBar progressBar;

    MyAdapter mAdapter;
    RecyclerView.LayoutManager layoutManager;
    MainActivityViewModel model;
    MainActivity activity;
    ArrayList<EventItem> events;

    public EventFragment() {
        // Required empty public constructor
    }

    public static EventFragment newInstance() {
        EventFragment fragment = new EventFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = (MainActivity) getActivity();

        model = new ViewModelProvider(requireActivity()).get(MainActivityViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event, container, false);
        initView(view);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        events = new ArrayList<>();
        mAdapter = new MyAdapter(events);
         mAdapter.setOnActionPerformedListener(new MyAdapter.OnActionPerformedListener() {
            @Override
            public void onClickEvent(String event, int id) {
                model.setIdEvent(id);
                model.setEvent(event);
                Toast.makeText(activity, "Seleccionado " + event, Toast.LENGTH_SHORT).show();
            }
        });
        recyclerView.setAdapter(mAdapter);

        getEvents();

        return view;
    }

    private void getEvents() {
        disableInteraction();
        GetEventsRequestTask getEventsRequestTask = new GetEventsRequestTask();
        getEventsRequestTask.setListener(new OnApiRequestTaskCompleted() {
            @Override
            public void onApiRequestTaskSucceed(Object object) {
                enableInteraction();

                ArrayList<EventItem> items = (ArrayList<EventItem>) object;
                events.clear();
                // Active items
                for (EventItem item: items) {
                    if (item.getHidden() == EventItem.STATE_SHOW) {
                        events.add(item);
                    }
                }
                 // Update list
                mAdapter.setData(events);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onApiRequestTaskFailed(Response response) {
                Toast.makeText(activity.getApplicationContext(), R.string.request_fail, Toast.LENGTH_SHORT).show();
                enableInteraction();
            }
        });
        getEventsRequestTask.execute();
    }

    private void initView(View view) {
        recyclerView = view.findViewById(R.id.rvEvents);
        progressBar = view.findViewById(R.id.progressBar);

        progressBar.setVisibility(View.GONE);
    }

    private void disableInteraction() {
        progressBar.setVisibility(View.VISIBLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void enableInteraction() {
        progressBar.setVisibility(View.GONE);
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}