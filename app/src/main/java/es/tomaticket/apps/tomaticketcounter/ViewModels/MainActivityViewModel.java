package es.tomaticket.apps.tomaticketcounter.ViewModels;

import androidx.lifecycle.ViewModel;

public class MainActivityViewModel extends ViewModel {
    public static final int NO_EVENT_ID = -100;

    private int idEvent = NO_EVENT_ID;
    private int count = NO_EVENT_ID;
    private String event = "";

    public int getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(int idEvent) {
        this.idEvent = idEvent;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
