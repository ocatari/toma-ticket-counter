package es.tomaticket.apps.tomaticketcounter.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import es.tomaticket.apps.tomaticketcounter.Models.EventItem;
import es.tomaticket.apps.tomaticketcounter.R;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private ArrayList<EventItem> mDataset;

    public interface OnActionPerformedListener {
        void onClickEvent(String event, int id);
    }

    private OnActionPerformedListener onActionPerformedListener;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;
        public MyViewHolder(View v) {
            super(v);
            textView = v.findViewById(R.id.tvEvent);
        }
    }

    public MyAdapter(ArrayList<EventItem> data) {
        this.mDataset = data;
    }

    public void setData(ArrayList<EventItem> data) {
        this.mDataset = data;
    }

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_event, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.textView.setText(mDataset.get(position).getEvent());

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onActionPerformedListener != null) {
                    onActionPerformedListener.onClickEvent(mDataset.get(position).getEvent(), mDataset.get(position).getId());
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setOnActionPerformedListener(OnActionPerformedListener onActionPerformedListener) {
        this.onActionPerformedListener = onActionPerformedListener;
    }

}
