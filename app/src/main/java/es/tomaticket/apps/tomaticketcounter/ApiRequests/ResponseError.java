package es.tomaticket.apps.tomaticketcounter.ApiRequests;


import java.util.LinkedHashMap;
import java.util.List;

public class ResponseError {

    private String message;
    private String error;
    private LinkedHashMap<String, List<String>> errors;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public LinkedHashMap<String, List<String>> getErrors() {
        return errors;
    }

    public void setErrors(LinkedHashMap<String, List<String>> errors) {
        this.errors = errors;
    }
}

