package es.tomaticket.apps.tomaticketcounter.Fragments;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import es.tomaticket.apps.tomaticketcounter.ApiRequests.AddEventCountRequestTask;
import es.tomaticket.apps.tomaticketcounter.ApiRequests.GetEventCountRequestTask;
import es.tomaticket.apps.tomaticketcounter.ApiRequests.OnApiRequestTaskCompleted;
import es.tomaticket.apps.tomaticketcounter.ApiRequests.Response;
import es.tomaticket.apps.tomaticketcounter.MainActivity;
import es.tomaticket.apps.tomaticketcounter.R;
import es.tomaticket.apps.tomaticketcounter.Utils.Style;
import es.tomaticket.apps.tomaticketcounter.ViewModels.MainActivityViewModel;

public class AddFragment extends Fragment {

    Button add;
    TextView count;
    ProgressBar progressBar;
    TextView event;

    MainActivityViewModel model;
    MainActivity activity;
    Handler handler = new Handler();
    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            Log.e("Handler", "Run");
            updateCount();

            handler.postDelayed(this, 5000);
        }
    };


    public AddFragment() {
        // Required empty public constructor
    }


    public static AddFragment newInstance() {
        AddFragment fragment = new AddFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = (MainActivity) getActivity();


        model = new ViewModelProvider(requireActivity()).get(MainActivityViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add, container, false);
        initView(view);


        handler.post(runnableCode);

        final Vibrator v = (Vibrator) activity.getSystemService(Context.VIBRATOR_SERVICE);
        final MediaPlayer mp = MediaPlayer.create(activity, R.raw.beep);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (model.getIdEvent() == MainActivityViewModel.NO_EVENT_ID) {
                    Toast.makeText(activity, "Necesario selecionar evento", Toast.LENGTH_SHORT).show();
                    return;
                }

                disableInteraction();
                String id = Integer.toString(model.getIdEvent());
                AddEventCountRequestTask addEventCountRequestTask = new AddEventCountRequestTask(id);
                addEventCountRequestTask.setListener(new OnApiRequestTaskCompleted() {
                    @Override
                    public void onApiRequestTaskSucceed(Object object) {
                        Integer c = (Integer) object;
                        count.setText(Integer.toString(c));

                        if (mp.isPlaying()) {
                            mp.pause();
                            mp.seekTo(0);
                        }
                        mp.start();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                        } else {
                            v.vibrate(500);
                        }

                        enableInteraction();
                    }

                    @Override
                    public void onApiRequestTaskFailed(Response response) {
                        Toast.makeText(activity, "Petición fallida", Toast.LENGTH_SHORT).show();
                        enableInteraction();
                    }
                });
                addEventCountRequestTask.execute();
            }
        });

        return view;
    }

    private void initView(View view) {
        add = view.findViewById(R.id.btnAdd);
        count = view.findViewById(R.id.tvCount);
        progressBar = view.findViewById(R.id.progressBar);
        event = view.findViewById(R.id.tvEvent);

        progressBar.setVisibility(View.GONE);

        int radious = 20;
        GradientDrawable pressedBg = Style.getStyle(R.color.colorPrimaryLight, radious, activity);
        GradientDrawable inactiveBg = Style.getStyle(R.color.colorPrimaryLight, radious, activity);
        GradientDrawable defaultBg = Style.getStyle(R.color.colorAddBtn, radious, activity);
        StateListDrawable stateListDrawable = Style.getStyleState(pressedBg, inactiveBg, defaultBg);
        add.setBackground(stateListDrawable);

        ColorStateList colorStateList = Style.getStyleColorState(R.color.textColorPrimary, R.color.textColorPrimary, activity);
        add.setTextColor(colorStateList);
    }

    private void updateCount() {
        if (model.getIdEvent() != MainActivityViewModel.NO_EVENT_ID) {
            String id = Integer.toString(model.getIdEvent());
            GetEventCountRequestTask getEventCountRequestTask = new GetEventCountRequestTask(id);
            getEventCountRequestTask.setListener(new OnApiRequestTaskCompleted() {
                @Override
                public void onApiRequestTaskSucceed(Object object) {
                    Integer c = (Integer) object;
                    count.setText(Integer.toString(c));
                }

                @Override
                public void onApiRequestTaskFailed(Response response) {

                }
            });
            getEventCountRequestTask.execute();

        }
        event.setText(model.getEvent());
    }

    private void disableInteraction() {
        progressBar.setVisibility(View.VISIBLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void enableInteraction() {
        progressBar.setVisibility(View.GONE);
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(runnableCode);

        super.onDestroy();
    }
}