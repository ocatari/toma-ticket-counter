package es.tomaticket.apps.tomaticketcounter.ApiRequests;

public interface OnApiRequestTaskCompleted {
    void onApiRequestTaskSucceed(Object object);
    void onApiRequestTaskFailed(Response response);
}
