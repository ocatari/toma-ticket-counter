package es.tomaticket.apps.tomaticketcounter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import es.tomaticket.apps.tomaticketcounter.Fragments.AddFragment;
import es.tomaticket.apps.tomaticketcounter.Fragments.EventFragment;
import es.tomaticket.apps.tomaticketcounter.Fragments.MinusFragment;
import es.tomaticket.apps.tomaticketcounter.Fragments.ResetFragment;

public class PagerAdapter extends FragmentStateAdapter {

    public PagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new EventFragment();
            case 1:
                return new AddFragment();
            case 2:
                return new MinusFragment();
            case 3:
                return new ResetFragment();
            default:
                return null;
        }
    }

    @Override
    public int getItemCount() {
        return 4;
    }
}
