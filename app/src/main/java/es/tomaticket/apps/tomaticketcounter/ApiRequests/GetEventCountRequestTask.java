package es.tomaticket.apps.tomaticketcounter.ApiRequests;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import java.util.HashMap;

public class GetEventCountRequestTask extends ApiRequestBaseTask {
    private static final String ROUTE = "/getCounter.php";

    private Response response;

    private OnApiRequestTaskCompleted listener;
    private HashMap<String, String> paramsReq;
    private int count;

    public GetEventCountRequestTask(String id) {
        paramsReq = new HashMap<String, String>();
        paramsReq.put("event_id", id);
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

            response = getServerResponse("GET", BASE_URL, ROUTE, null, paramsReq, null);

            try {
                if (response.getCode() == 200) {
                    JsonObject jsonResponse = gson.fromJson(response.getBody(), JsonObject.class);
                    if (jsonResponse.get("success").getAsBoolean()) {
                        count = jsonResponse.get("count").getAsInt();
                        return true;
                    } else {
                        return false;
                    }
                } else if (response.getCode() >= 400) {
                    Log.e("RESPONSE",  response.getBody());
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        if (success) {
            listener.onApiRequestTaskSucceed(count);
        } else {
            listener.onApiRequestTaskFailed(response);
        }
    }

    public void setListener(OnApiRequestTaskCompleted listener) {
        this.listener = listener;
    }

}
