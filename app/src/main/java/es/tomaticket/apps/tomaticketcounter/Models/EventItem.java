package es.tomaticket.apps.tomaticketcounter.Models;

import com.google.gson.annotations.SerializedName;

public class EventItem {
    public static int STATE_SHOW = 0;

    @SerializedName("Id") Integer id;
    @SerializedName("Cementerio") String event;
    @SerializedName("Contador") Integer count;
    @SerializedName("hidden") Integer hidden;

    public Integer getId() {
        return id;
    }

    public String getEvent() {
        return event;
    }

    public Integer getCount() {
        return count;
    }

    public Integer getHidden() {
        return hidden;
    }
}
