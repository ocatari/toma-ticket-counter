package es.tomaticket.apps.tomaticketcounter.ApiRequests;

import android.os.AsyncTask;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import es.tomaticket.apps.tomaticketcounter.BuildConfig;

public class ApiRequestBaseTask extends AsyncTask<Void, Void, Boolean> {

    public static final String BASE_URL = "http://flotas.officialtaxitenerife.com/tomaticketcounter/requests";

    public static final int CONNECT_TIMEOUT = 5000;
    public static final int READ_TIMEOUT = 15000;

    private static final String TIMEOUT_ERROR = "Timeout error. The server does no respond.";
    private static final String CONNECTION_ERROR = "Connection Error. Cannot connect to server.";
    private static final String INVALID_RESPONSE = "Invalid server response.";

    public ApiRequestBaseTask() {}

    @Override
    protected Boolean doInBackground(Void... params) {
        return null;
    }

    protected Response getServerResponse(String method, String api, String route, HashMap<String, String> headers, HashMap<String, String> urlParams, String jsonBody) {
        BufferedReader reader = null;
        Response response = new Response();
        HttpURLConnection connection = null;
        Gson gson = new Gson();

        try {

            if (headers == null) {
                headers = new HashMap<>();
            }

            if (urlParams == null) {
                urlParams = new HashMap<>();
            }

            String urlParamsString = "";

            StringBuilder sb = new StringBuilder();
            int i = 1;
            for (Map.Entry<String, String> entry : urlParams.entrySet()) {
                if (i >= 2) {
                    sb.append("&");
                }
                sb.append(entry.getKey());
                sb.append("=");
                sb.append(entry.getValue());
                i++;
            }
            urlParamsString = "?" + sb.toString();

            URL url = new URL(api + route + urlParamsString);

            connection = (HttpURLConnection) url.openConnection();
            connection.setUseCaches(false);
            connection.setRequestMethod(method);
            connection.setConnectTimeout(CONNECT_TIMEOUT);
            connection.setReadTimeout(READ_TIMEOUT);
            connection.setRequestProperty("Accept", "application/json");

            // Set headers
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                connection.setRequestProperty(entry.getKey(), entry.getValue());
            }

            // Set json body
            if (jsonBody != null) {
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                connection.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
                wr.write(jsonBody);
                wr.close();
            }

            if (connection.getResponseCode() == 200 || connection.getResponseCode() == 201) {
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            } else {
                reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            }

            response.setCode(connection.getResponseCode());
            response.setMessage(connection.getResponseMessage());

            String contentType = connection.getHeaderField("Content-Type");

            if (contentType != null && (contentType.contains("application/json") || contentType.contains("text/html"))) {
                sb = new StringBuilder();
                String responseString;
                while ((responseString = reader.readLine()) != null) {
                    sb.append(responseString);
                }
                response.setBody(sb.toString());
            } else {
                response.setCode(connection.getResponseCode());
                ResponseError responseError = new ResponseError();
                responseError.setError(INVALID_RESPONSE);
                responseError.setMessage(INVALID_RESPONSE);
                response.setResponseError(responseError);
            }

            if (connection.getResponseCode() != 200) {
                //response.setResponseError(gson.fromJson(response.getBody(), ResponseError.class));
            }

        } catch (java.net.SocketTimeoutException e) {
            e.printStackTrace();
            ResponseError responseError = new ResponseError();
            responseError.setError(TIMEOUT_ERROR);
            responseError.setMessage(TIMEOUT_ERROR);
            response.setResponseError(responseError);
            response.setCode(601);

            return response;

        } catch (Exception e) {
            e.printStackTrace();
            ResponseError responseError = new ResponseError();
            responseError.setError(CONNECTION_ERROR);
            responseError.setMessage(CONNECTION_ERROR);
            response.setResponseError(responseError);
            response.setCode(602);
            return response;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return response;
    }

}
