package es.tomaticket.apps.tomaticketcounter.ApiRequests;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;

import es.tomaticket.apps.tomaticketcounter.Models.EventItem;

public class GetEventsRequestTask extends ApiRequestBaseTask {
    private static final String ROUTE = "/getEvents.php";

    private Response response;

    private OnApiRequestTaskCompleted listener;
    private ArrayList<EventItem> events;

    public GetEventsRequestTask() {
        events = new ArrayList<>();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

            response = getServerResponse("GET", BASE_URL, ROUTE, null, null, null);

            try {
                if (response.getCode() == 200) {
                    JsonObject jsonResponse = gson.fromJson(response.getBody(), JsonObject.class);
                    if (jsonResponse.get("success").getAsBoolean()) {
                        String content = jsonResponse.get("events").getAsJsonArray().toString();
                        EventItem[] array = gson.fromJson(content, EventItem[].class);
                        events.addAll(Arrays.asList(array));
                        return true;
                    } else {
                        return false;
                    }
                } else if (response.getCode() >= 400) {
                    Log.e("RESPONSE",  response.getBody());
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        if (success) {
            listener.onApiRequestTaskSucceed(events);
        } else {
            listener.onApiRequestTaskFailed(response);
        }
    }

    public void setListener(OnApiRequestTaskCompleted listener) {
        this.listener = listener;
    }
}
