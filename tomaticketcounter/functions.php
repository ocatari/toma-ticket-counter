<?php

function connectDB () {
    $connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASSWD, DB_NAME, DB_PORT);

    if (!$connection) {
        echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
        echo "errno de depuración: " . mysqli_connect_errno() . PHP_EOL;
        echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
        exit;
    } 
    
    return $connection;
}

function get_event_count($event_id) {    
    $conn = connectDB();     
    $sql = "SELECT * FROM cementerios WHERE Id=$event_id";
    
    $result = mysqli_query($conn, $sql);

	if ($result && mysqli_num_rows($result) > 0) {
       
        $row = mysqli_fetch_assoc($result);
        $count = $row['Contador'];
        
		return $count;
	}
    return null;
}


function add_event_count($event_id) {
    $conn = connectDB();

    // Update count +1
    $sql = "UPDATE cementerios SET Contador=Contador+1 WHERE Id=$event_id";
    $result =  mysqli_query($conn, $sql);
    
    if ($result) {
        //  Get updated count
        $updated_count = get_event_count($event_id);

        // Generate log. Operation add = 1, susbtract = 0
        $data = array(
            'id_event' => $event_id,
            'operation' => 1,
            'count' => $updated_count
        );
        event_log($data);

        return $data['count'];
    }

    return null;
}

function subtract_event_count($event_id) {
    $conn = connectDB();

    // Update count -1
    $sql = "UPDATE cementerios SET Contador=Contador+-1 WHERE Id=$event_id";
    $result =  mysqli_query($conn, $sql);
    
    if ($result) {
        //  Get updated count
        $updated_count = get_event_count($event_id);

        // Generate log. Operation add = 1, susbtract = 0
        $data = array(
            'id_event' => $event_id,
            'operation' => 0,
            'count' => $updated_count
        );
        event_log($data);

        return $data['count'];
    }

    return null;
}

function reset_event_count($event_id, $reset_password) {
    if(comprobar_password($reset_password)) {
		$conn = connectDB();

		// Update count -> 0
		$sql = "UPDATE cementerios SET Contador=0 WHERE Id=$event_id";
		$result =  mysqli_query($conn, $sql);

		if ($result) {
			//  Get updated count
			$updated_count = get_event_count($event_id);

			// Generate log. Operation add = 1, susbtract = 0, reset = -1
			$data = array(
				'id_event' => $event_id,
				'operation' => -1,
				'count' => $updated_count
			);
			event_log($data);

			return $data['count'];
		}
	}
	else { return "Contraseña incorrecta"; }

    return null;
}

function comprobar_password($password) {
	$password_reset = array("tomatoma", "Tomatoma");
	return in_array($password, $password_reset);
}

function event_log($datos) {    
    $conn = connectDB();
    $sql = "INSERT INTO datos (IdCementerio, EntradaoSalida, Fecha, Contador)
    VALUES (".$datos['id_event'].",".$datos['operation'].",NOW(),".$datos['count'].")";

    $result =  mysqli_query($conn, $sql);

    if ($result)
        return true;
    else
        return null;
}

function get_events() {    
    $conn = connectDB();     
    $sql = "SELECT * FROM cementerios";
    
    $result = mysqli_query($conn, $sql);
    $events = array();
    
	if ($result && mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            array_push($events, $row);
        }        
		return $events;
	}
    return null;
}


?>
