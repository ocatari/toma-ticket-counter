<?php

$path = "../";
include($path . 'server.php');
include($path . 'functions.php');

if (isset($_REQUEST['event_id'])) {	
    $count = reset_event_count($_REQUEST['event_id'], $_REQUEST['reset_password']);   

    if ($count != null) {
		if ($count == "Contraseña incorrecta") {
			$response = array(
				'success' => false,
				'error' => $count
        );
		} else {
			$response = array(
				'success' => true,
				'count' => $count
			);	
		}
    } else {
        $response = array(
            'success' => false,
            'error' => "Unexpected error"
        );
    }	
} else {	
	$response = array(
		'success' => false,
		'error' => "No event id"
	);
}

echo json_encode($response);

?>