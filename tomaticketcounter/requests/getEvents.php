<?php

$path = "../";
include($path . 'server.php');
include($path . 'functions.php');

$events = get_events();   

if ($events != null) {
    $response = array(
        'success' => true,
        'events' => $events
    );	
} else {
    $response = array(
        'success' => false,
        'error' => "Unexpected error"
    );
}	


echo json_encode($response);

?>