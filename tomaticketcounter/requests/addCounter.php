<?php

$path = "../";
include($path . 'server.php');
include($path . 'functions.php');

if (isset($_REQUEST['event_id'])) {	
    $count = add_event_count($_REQUEST['event_id']);   

    if ($count != null) {
        $response = array(
            'success' => true,
            'count' => $count
        );	
    } else {
        $response = array(
            'success' => false,
            'error' => "Unexpected error"
        );
    }	
} else {	
	$response = array(
		'success' => false,
		'error' => "No event id"
	);
}

echo json_encode($response);

?>